if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
  # XKB_DEFAULT_LAYOUT=us exec sway
fi

EDITOR=vim
_JAVA_AWT_WM_NONREPARENTING=1
MOZ_ENABLE_WAYLAND=1
XDG_CURRENT_DESKTOP=sway
XDG_SESSION_TYPE=wayland
WLR_DRM_NO_MODIFIERS=1
MOZ_USE_XINPUT2=1
# so iex has history across sessions
ERL_AFLAGS="-kernel shell_history enabled"

