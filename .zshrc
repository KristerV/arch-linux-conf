# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=100000
SAVEHIST=100000
setopt extendedglob
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/krister/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

. /opt/asdf-vm/asdf.sh

git config --global alias.lg "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"

source /etc/profile.d/vte.sh # so terminal new tab would open in same dir
#source ~/.secretsrc

alias gst="git status"
alias gd="git diff -w -- . ':!package-lock.json' ':!mix.lock'"
alias gaa="git add --all"
alias ls='ls --color=auto'
alias home='git --work-tree=$HOME --git-dir=$HOME/.home'
alias gpprod='git checkout production && git merge master && git push && git checkout master && git push'
alias gpdemo='git checkout demo && git merge master && git push && git checkout master && git push'
alias gclean="git checkout . && git clean -fd"
export PATH=/home/krister/.meteor:$PATH
export PATH=/home/krister/.elixir-ls:$PATH
eval $(thefuck --alias)
alias merge-screenrec='~/code/arch-conf/merge-recordings.sh'
alias lb='cd ~/code/livebook && LIVEBOOK_HOME=/home/krister/code/livebook livebook server @home'
