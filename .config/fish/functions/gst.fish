git config --global alias.lg "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"

alias gst="git status"
alias gd="git diff -- . ':!package-lock.json'"
alias gaa="git add --all"
