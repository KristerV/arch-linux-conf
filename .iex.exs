Mix.install([:decimal, {:shorter_maps, github: "Galagora/shorter_maps"}, {:timex, "~> 3.0"}, {:req, "~> 0.3"}])
alias Decimal, as: D
import ShorterMaps
