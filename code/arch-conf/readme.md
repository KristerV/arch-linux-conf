This guide assumes you've used the official archinstall to install arch.

# Install git

pacman -S --needed --noconfirm git

## Setup configs
git clone https://gitlab.com/KristerV/arch-linux-conf.git
cp -r arch-linux-conf/* .
cp -r arch-linux-conf/.* .
mv .git .home

## Install everything
./code/arch-conf/install_packages.sh

## ZSH configuration
chsh -s /bin/zsh
cat ~/.zimrc | sudo tee -a /etc/zsh/zimrc
sudo su
chsh -s /bin/zsh
zimfw install
exit
# read https://aur.archlinux.org/packages/zsh-zim-git/ to install modules in .zimrc
# must run ''zimfw install' as root after every ZIM update.. omg

# https://wiki.archlinux.org/title/Bluetooth#Wake_from_suspend
xhost si:localuser:root

# Autostart bluetooth
# /etc/bluetooth/main.conf
# [Policy]
# AutoEnable=true

## Autologin (unless you use greetd, GMD or something)
yay -S --noconfirm mingetty
sudo mkdir /etc/systemd/system/getty@tty1.service.d/
sudo echo "[Service]
ExecStart=
ExecStart=-/usr/bin/agetty --autologin krister --noclear %I $TERM" > /etc/systemd/system/getty@tty1.service.d/override.conf

## Sudo

Add these lines to visudo
```
Defaults passwd_timeout=0
Defaults timestamp_timeout=25
```
And make `wheel` sudo.

## Elixir

May need to reboot before this.

```
asdf plugin-add erlang https://github.com/asdf-vm/asdf-erlang.git
export KERL_CONFIGURE_OPTIONS="--disable-debug --without-javac"
asdf plugin-add elixir https://github.com/asdf-vm/asdf-elixir.git
asdf plugin add nodejs https://github.com/asdf-vm/asdf-nodejs.git
```

## Screen sharing
yay -S pipewire xdg-desktop-portal xdg-desktop-portal-wlr pipewire-media-session libpipewire02 --noconfirm
systemctl enable --user pipewire
systemctl --user enable --now pipewire-media-session.service
# chrome://flags/#enable-webrtc-pipewire-capturer
# detailed instrucitons: https://elis.nu/blog/2021/02/detailed-setup-of-screen-sharing-in-sway/
# make sure XDG_CURRENT_DESKTOP=sway
