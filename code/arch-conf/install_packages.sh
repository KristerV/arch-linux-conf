sudo pacman -S --needed --noconfirm - < packages/basic.txt

# Install yay
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
cd ..
rm -rf yay
# these next 3 commands don't do anything.. not sure what they are
yay -Y --gendb
yay -Syu --devel
yay -Y --devel --save
sudo sed -i '/Color/s/^#//' /etc/pacman.conf

# Install packages
yay -S --needed --noconfirm - < packages/sway.txt
yay -S --needed --noconfirm - < packages/long_running.txt

# Further setup
yay -R gnu-free-fonts # messes up braille fontsin waybar
gsettings set org.mate.applications-terminal exec gnome-terminal
sudo systemctl enable reflector.service reflector.timer
sudo systemctl enable NetworkManager
sudo systemctl enable bluetooth
sudo systemctl enable docker
sudo systemctl enable keyd
sudo ln -s /home/krister/.config/keyd/default.conf /etc/keyd/default.conf
sudo usermod -a -G video krister
sudo usermod -a -G docker krister
mkdir -p ~/Documents/screengrab
